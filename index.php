<!DOCTYPE html>
<!--
                                    Mustafa Omran 
                                Single server web application to calculate leap year 
                                a year that has 366 days, Feb has 29 day . . . . . .
-->
<html>
 <!---Header -->   
<?php include './files/head.php'; ?>
    
<!-- Process--> 
<?php include './files/body.php';?>
 
<!-- Footer -->
<?php include './files/footer.php';?>
</html>
