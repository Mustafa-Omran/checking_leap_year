    <body class="w3-container w3-light-grey">
        
        <form action="" method="get" class="w3-container" style="width:50%;margin:0 auto ">
            <div class="w3-center w3-xxlarge w3-text-green w3-opacity w3-pale-green" style="border:2px #3a3a3a solid">checking a Leap year ! <i class="fa fa-xing"></i></div><br>
            <input type="text" name="year" value="" class="w3-center w3-input w3-text-green w3-border w3-border-dark-grey w3-round-jumbo"><br>
            <input type="submit" value="Check" class="w3-center w3-input w3-btn-block w3-text-green w3-opacity w3-large w3-pale-green w3-border-green w3-round-jumbo">
        </form>

       <div class="w3-center w3-xlarge w3-text-green" style="width:50%;margin:0 auto;margin-top:25px">
           
       <!-- Process here checks if the input is a leap year = 366 days or not !  -->
          <?php
          function leap()
          {
          $x=$_GET['year'];
              if($x%4>0)
              {
              echo 'No';
              }
              elseif ($x%100>0) 
              {
              echo 'yes';
              }
              elseif ($x%400>0) 
              {
              echo 'No';
              }
          }
          leap();
      ?>
        </div>